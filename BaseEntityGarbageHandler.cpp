//
// Created by Admin on 8/17/2017.
//

#include <iostream>
#include "BaseEntityGarbageHandler.h"

void BaseEntityGarbageHandler::MarkForDelete(BaseEntity *be) {
    garbagePile.insert(be);
}

void BaseEntityGarbageHandler::DeleteAllMarked() {
    for(BaseEntity *b:garbagePile){
        if(b != NULL){
            std::cout << "Garbage deleting entity! " << std::endl;
            delete b;
        }
    }
    garbagePile.clear();
}
