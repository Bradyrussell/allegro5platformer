//
// Created by Admin on 8/14/2017.
//

#include <Box2D.h>
#include <allegro5/allegro.h>
#include "EntityBall.h"
#include "UserEvents.h"
#include "GameObjectData.h"


EntityBall::EntityBall(b2World* w,Renderer *renderer) {
    world = w; // grab the world reference
    Register(renderer,Z_GENERICENTITY); // z index genericentity

    b2BodyDef ballBodyDef; // define starting parameters for player body
    ballBodyDef.type = b2_dynamicBody; // dynamic body
    ballBodyDef.position.Set(0, 0); //starting position
    ballBodyDef.angle = 0; //starting angle

    ballBodyDef.fixedRotation = false;

    //set user data
    gameobjectdata *god = new gameobjectdata;
    god->type = OBJECT_ENTITYBALL;
    god->data = this;
    ballBodyDef.userData = god;

    body=world->CreateBody(&ballBodyDef);

    b2CircleShape ballShape;
    ballShape.m_p.Set(0,0);
    ballShape.m_radius = .5;

    b2FixtureDef circleFixtureDef;
    circleFixtureDef.shape = &ballShape;
    circleFixtureDef.restitution = .5;
    circleFixtureDef.density = 1;
    body->CreateFixture(&circleFixtureDef);

    //SetDrawBody(body); // make sure the drawing and physics are talking about the same body

}

void EntityBall::Draw() {
    //do draw stuff
    if(body != NULL) {

        b2Vec2 pos = body->GetPosition();
        float x = pos.x * PIX_METER;
        float y = pos.y * PIX_METER;

        al_draw_rotated_bitmap(current_sprite, half_w, half_h, x + half_w, y + half_h, body->GetAngle(), 0);
    }
    return;
}

EntityBall::~EntityBall() {
    //Drawable::~Drawable();
}
