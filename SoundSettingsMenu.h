//
// Created by Admin on 9/9/2017.
//

#ifndef BOX2DPLATFORM_SOUNDSETTINGSMENU_H
#define BOX2DPLATFORM_SOUNDSETTINGSMENU_H


#include "BaseMenu.h"

class SoundSettingsMenu : public BaseMenu {
public:
    SoundSettingsMenu(agui::Gui *gui);

private:
    void Create() override;

    MenuMiscListener soundsettings_listener;

    // begin defining widgets
    agui::FlowLayout flow;
    agui::Button stop_button;

    agui::Button save_button;

    agui::Button close_button;
    agui::TextField label;

    agui::Slider volume_slider;
    // end defining widgets

};


#endif //BOX2DPLATFORM_SOUNDSETTINGSMENU_H
