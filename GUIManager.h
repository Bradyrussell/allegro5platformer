//
// Created by Admin on 8/18/2017.
//

#ifndef BOX2DPLATFORM_GUIMANAGER_H
#define BOX2DPLATFORM_GUIMANAGER_H

#include <Agui/Agui.hpp>
#include <Agui/Backends/Allegro5/Allegro5.hpp>

#include <Agui/Widgets/Button/Button.hpp>
#include <Agui/Widgets/CheckBox/CheckBox.hpp>
#include <Agui/Widgets/DropDown/DropDown.hpp>
#include <Agui/Widgets/TextField/TextField.hpp>
#include <Agui/Widgets/Frame/Frame.hpp>
#include <Agui/Widgets/RadioButton/RadioButton.hpp>
#include <Agui/Widgets/RadioButton/RadioButtonGroup.hpp>
#include <Agui/Widgets/Slider/Slider.hpp>
#include <Agui/Widgets/TextBox/ExtendedTextBox.hpp>
#include <Agui/Widgets/Tab/TabbedPane.hpp>
#include <Agui/Widgets/ListBox/ListBox.hpp>
#include <Agui/Widgets/ScrollPane/ScrollPane.hpp>
#include <Agui/FlowLayout.hpp>
#include <iostream>
#include <set>
#include "UserEvents.h"
#include "BaseMenu.h"

// my handler for AGUI
//actual menus themselves will have their own classes? at least their own functions

/* agui widgets:
 * 	agui::FlowLayout flow;
	agui::Button button;
	agui::CheckBox checkBox;
	agui::DropDown dropDown;
	agui::TextField textField;
	agui::Frame frame;
	agui::Gui* mGui;
	agui::RadioButton rButton;
	agui::RadioButtonGroup rGroup;
	agui::Slider slider;
	agui::ExtendedTextBox exTextBox;
	agui::TabbedPane tabbedPane;
	agui::Tab tab;
	agui::ListBox listBox;
	agui::ScrollPane scrollPane;
	agui::Button scrollButtons;
 */

class BaseMenu;

class GUIManager {
public:
    agui::Gui *gui = nullptr;
    agui::Allegro5Input* inputHandler = nullptr;
    agui::Allegro5Graphics* graphicsHandler = nullptr;
    agui::Font * defaultFont = nullptr;

    std::map <std::string,BaseMenu*> active_menus; // maps

    bool menu_has_mouse=false; // use this to make sure we dont do game actions when clicking menu stuff
    bool menu_has_keys=false; // does menu have the keyboard

    void Init();
    virtual ~GUIManager();

    void HandleInput(ALLEGRO_EVENT evt);
    void Clear();
    void Draw();
    void Update();

    BaseMenu * CreateMenu(BaseMenu *menu, const char *name); // set up a menu using the provided derivative of BaseMenu
    BaseMenu * GetMenu(std::string name);
    void RemoveMenu(std::string name);
    std::list<std::string> GetMenuList();
    int GetMenuCount();
    void ValidateMenus();

    bool isUsingMouse() const;
    void setUsingMouse(bool menu_has_mouse);
    bool isUsingKeys() const;
    void setUsingKeys(bool menu_has_keys);
    void setUsingMouseKeys(bool menu_has_both);
};




#endif //BOX2DPLATFORM_GUIMANAGER_H
