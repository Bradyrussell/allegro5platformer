//
// Created by Admin on 8/14/2017.
//

#include <allegro5/allegro.h>
#include <iostream>
#include "Drawable.h"

void Drawable::Draw() {
 // should be overridden
    std::cout << "[error] Drawable::Draw() was not overridden here!" << std::endl;
}

void Drawable::SetSprite(ALLEGRO_BITMAP *s) {
    current_sprite=s;
    half_w = al_get_bitmap_width(current_sprite) / 2;
    half_h = al_get_bitmap_height(current_sprite) / 2;
    return;
}

ALLEGRO_BITMAP *Drawable::GetSprite() const{
    return current_sprite;
}

void Drawable::Register(Renderer *renderer) {     // to make a drawable you must pass it a reference to a list of drawables. or you could pass nullptr...
    renderer->RegisterDrawable(this);
    owner_renderer=renderer; // gotta save reference so we can remove ourselves on destruction
    return;
}

Drawable::Drawable() {
    z_index=0;
    current_sprite = al_create_bitmap(16, 16);
    half_w = al_get_bitmap_width(current_sprite) / 2;
    half_h = al_get_bitmap_height(current_sprite) / 2;
}

int Drawable::getZindex() const {
    return z_index;
}

void Drawable::setZindex(int z_index) {
    Drawable::z_index = z_index;
}

void Drawable::Register(Renderer *renderer, int z) {
    setZindex(z);
    Register(renderer);
    return;
}

Drawable::~Drawable() {
    owner_renderer->UnregisterDrawable(this);
    std::cout << "Drawable::~Drawable() Called. z:" <<  z_index << std::endl;
    return;
}


