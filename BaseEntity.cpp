//
// Created by Admin on 8/14/2017.
//

#include <Dynamics/b2Fixture.h>
#include <allegro5/allegro.h>
#include <iostream>
#include "BaseEntity.h"

#define SCREENWIDTH 640
#define SCREENHEIGHT 480
#define FPS_LIMIT 60
#define PIX_METER 16;
#define TITLE "Platformer 0.0.1"

b2Vec2 BaseEntity::GetPosition() {
    return body->GetPosition();
}

float BaseEntity::GetX() {
    return body->GetPosition().x;
}

float BaseEntity::GetY() {
    return body->GetPosition().y;
}

void BaseEntity::SetWorld(b2World *w) {
    world = w;
    return;
}

b2World *BaseEntity::GetWorld() {
    return world;
}


void BaseEntity::SetPosition(float x, float y) {
    body->SetTransform(b2Vec2(x,y),body->GetAngle());
    return;
}

void BaseEntity::SetPosition(b2Vec2 b2v) {
    body->SetTransform(b2v,body->GetAngle());
}

BaseEntity::~BaseEntity() {
    world->DestroyBody(body);
    std::cout << "Destroyed body!" << std::endl;
}



