//
// Created by Admin on 8/13/2017.
//

#ifndef BOX2DPLATFORM_STATICTERRAINBLOCK_H
#define BOX2DPLATFORM_STATICTERRAINBLOCK_H


#include "Drawable.h"

class StaticTerrainBlock: public Drawable {
public:
    b2World *world;
    b2Body *body;
    int type;

    void Cleanup();

    virtual ~StaticTerrainBlock();

    virtual void Draw();


    StaticTerrainBlock(b2World *w, Renderer* renderer, float x, float y, int t);


};


#endif //BOX2DPLATFORM_STATICTERRAINBLOCK_H
