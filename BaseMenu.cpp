//
// Created by Admin on 9/9/2017.
//

#include "BaseMenu.h"

BaseMenu::BaseMenu(agui::Gui *gui) {
    BaseMenu::gui=gui;
}

void BaseMenu::RegisterWidget(std::string id, agui::Widget *w) {
    if(w!=nullptr) {
        id_map.insert(std::pair<std::string, agui::Widget *>(id, w));
    }
}

agui::Widget *BaseMenu::LookupWidget(std::string id) {
    agui::Widget *r;
    try {
        r=id_map.at(id);
    } catch(std::exception &e){
        return nullptr;
    }
    return r;
}

void BaseMenu::UnregisterWidget(std::string id) {
    id_map.erase(id);
}

BaseMenu::~BaseMenu() {
 std::cout << "~BaseMenu()... Better have deleted references to the object!"<<std::endl;
}

void BaseMenu::Create() {
    std::cout << "Creating basemenu!"<<std::endl;

    // to be overridden
    gui->add(&DEFAULTMENU_flow);

    DEFAULTMENU_flow.add(&DEFAULTMENU_text);
    DEFAULTMENU_text.setSize(140,20);
    DEFAULTMENU_text.setText("BaseMenu.");
    DEFAULTMENU_text.setFontColor(agui::Color(255,0,0));
    DEFAULTMENU_text.setBackColor(agui::Color(0,0,0,0));
    DEFAULTMENU_text.setReadOnly(true);
    DEFAULTMENU_text.setSelectable(false);
    RegisterWidget("text",&DEFAULTMENU_text);

    DEFAULTMENU_flow.add(&DEFAULTMENU_button2);
    DEFAULTMENU_button2.setSize(20,20);
    DEFAULTMENU_button2.setText("X");
    DEFAULTMENU_button2.addActionListener(&DEFAULTMENU_listener);
    RegisterWidget("close",&DEFAULTMENU_button2);

}

const std::string &BaseMenu::GetName() const {
    return menu_name;
}

void BaseMenu::SetName(const std::string &menu_name) {
    BaseMenu::menu_name = menu_name;
}

agui::Gui *BaseMenu::GetGui() const {
    return gui;
}

void BaseMenu::SetGui(agui::Gui *gui) {
    BaseMenu::gui = gui;
}

std::string BaseMenu::ReverseLookupWidget(agui::Widget *w) {
    for(std::pair<std::string, agui::Widget *>pair:id_map){
        if(pair.second == w){
            return pair.first;
        }
    }
    return "";
}

void BaseMenu::Destroy() {
    gui->flagWidget(&DEFAULTMENU_flow);
    gui->flagWidget(&DEFAULTMENU_button2);
    gui->flagWidget(&DEFAULTMENU_text);
    gui->destroyFlaggedWidgets();
}
