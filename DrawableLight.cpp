//
// Created by Admin on 8/17/2017.
//

#include <iostream>
#include "DrawableLight.h"

const b2Vec2 &DrawableLight::getPosition() const {
    return position;
}

void DrawableLight::setPosition(const b2Vec2 &position) {
    DrawableLight::position = position;
}

DrawableLight::DrawableLight(Renderer *renderer) {
    gradient=nullptr;
    Register(renderer);
}

//void DrawableLight::Draw() {
//    //Drawable::Draw();
//}

void DrawableLight::Register(Renderer *renderer) {
    renderer->RegisterLight(this);
    setRenderer(renderer);  // forgetting this caused crash on delete that frustrated me for an entire day 8/17/17
    setGradient(renderer->GetLightGradient());
}

Renderer *DrawableLight::getRenderer() const {
    return owner_renderer;
}

void DrawableLight::setRenderer(Renderer *owner_renderer) {
    DrawableLight::owner_renderer = owner_renderer;
}

ALLEGRO_BITMAP *DrawableLight::getGradient() const {
    return gradient;
}

void DrawableLight::setGradient(ALLEGRO_BITMAP *gradient) {
    DrawableLight::gradient = gradient;
}

void DrawableLight::Draw() {
    if(gradient!= nullptr) {
        float px, py, pw, ph, sw, sh;
        px = position.x * PIX_METER;
        py = position.y * PIX_METER;
        //ph = getIntensity()*PIX_METER;
        //pw = intensity*PIX_METER;

        sw = al_get_bitmap_width(gradient) / 2;
        sh = al_get_bitmap_height(gradient) / 2;
        //al_draw_bitmap(gradient,px,py,0);
        //al_draw_scaled_bitmap(gradient,0,0,64,64,px,py,pw,ph,0);
        al_draw_scaled_rotated_bitmap(gradient, sw, sh, px, py, getIntensity(), getIntensity(), 0, 0);
        //std::cout << "Drawing light source at: " << px << "," << py << std::endl;
    }
}

float DrawableLight::getIntensity() const {
    return intensity;
}

void DrawableLight::setIntensity(float intensity) {
    DrawableLight::intensity = intensity;
}

DrawableLight::DrawableLight(Renderer *renderer, b2Vec2 position, float intensity) {
    DrawableLight::position = position;
    DrawableLight::intensity = intensity;
    gradient=nullptr;
    Register(renderer);
}

DrawableLight::~DrawableLight() {
    std::cout << "DrawableLight::~DrawableLight() called." << std::endl;
    //owner_renderer->UnregisterLight(this);
    if(owner_renderer != nullptr) {
        owner_renderer->light_list.erase(this);
    }
}
