//
// Created by Admin on 9/9/2017.
//

#ifndef BOX2DPLATFORM_USEREVENTAGUIACTIONLISTENER_H
#define BOX2DPLATFORM_USEREVENTAGUIACTIONLISTENER_H


#include <Agui/Agui.hpp>
#include <Agui/Widgets/Slider/SliderListener.hpp>
#include "UserEvents.h"

class MenuButtonListener: public agui::ActionListener{
    public:
        void actionPerformed(const agui::ActionEvent &evt) override {
            auto *dat = new UserEvents::GUIEventData;
            dat->type = GUI_ACTION_BUTTON;
            dat->data = evt.getSource(); // hopefully this gives what widget action occured on
            UserEvents::ThrowGUIActionEvent(dat);
        }
    };


class MenuMiscListener: public agui::ActionListener{
public:
    void actionPerformed(const agui::ActionEvent &evt) override {
        auto *dat = new UserEvents::GUIEventData;
        dat->type = GUI_ACTION_MISC;
        dat->data = evt.getSource();
        UserEvents::ThrowGUIActionEvent(dat);
    }
};

//class MenuListener: public agui::ActionListener{
//public:
//    GUI_action_types given_type;
//
//    explicit MenuListener(GUI_action_types t) {given_type=t;}
//
//    void actionPerformed(const agui::ActionEvent &evt) override {
//        auto *dat = new UserEvents::GUIEventData;
//        dat->type = given_type;
//        dat->data = evt.getSource();
//        UserEvents::ThrowGUIActionEvent(dat);
//    }
//};
//class MenuSliderListener: public agui::SliderListener{
//public:
////    void actionPerformed(const agui::ActionEvent &evt) override {
////        auto *dat = new UserEvents::GUIEventData;
////        dat->type = GUI_ACTION_MISC;
////        dat->data = evt.getSource();
////        UserEvents::ThrowGUIActionEvent(dat);
////    }
//};



#endif //BOX2DPLATFORM_USEREVENTAGUIACTIONLISTENER_H
