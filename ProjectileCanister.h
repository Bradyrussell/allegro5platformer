//
// Created by Admin on 8/17/2017.
//

#ifndef BOX2DPLATFORM_PROJECTILECANISTER_H
#define BOX2DPLATFORM_PROJECTILECANISTER_H


#include "BaseProjectile.h"
#include "Drawable.h"
#include <Box2D.h>

class ProjectileCanister : public BaseProjectile, public Drawable{
public:
    DrawableLight *canlight;

    ProjectileCanister(b2World *w,Renderer *renderer);
    virtual void Draw();

    virtual ~ProjectileCanister();
};



#endif //BOX2DPLATFORM_PROJECTILECANISTER_H
