//
// Created by Admin on 8/15/2017.
//

#include <algorithm>
#include <iostream>
#include "Renderer.h"

void Renderer::RegisterDrawable(Drawable *d) {
//    drawable_list.push_back(d);  // old way  - requires resort every time. no thanks
//    assume_sorted=false; // we clearly are no longer sorted

        drawable_list.push_back(d);  // old way  - requires resort every time
        assume_sorted=false; // we clearly are no longer sorted

    return;
}

void Renderer::UnregisterDrawable(Drawable *d) {  // im not sure if this works :/
    if(d != NULL) {
        auto it = std::find(drawable_list.begin(), drawable_list.end(), d);
        if(it != drawable_list.end()) {
            std::cout << "Drawable (z:"<< (*it)->z_index << ") successfully unregistered. " << std::endl;
            drawable_list.erase(it);
        } else {
            std::cout << "Drawable (z:"<< (*it)->z_index << ") NOT FOUND. " << std::endl;
        }
    }
    return;
}

/*void Renderer::RenderFrameUnordered() {
    for(Drawable *d:drawable_list){
        if(d != NULL){
            d->Draw();
        }
    }
    return;
}*/

void Renderer::RenderFrame(){
    if(!assume_sorted) { // if not sorted then sort by z index
        std::sort(drawable_list.begin(), drawable_list.end(), [](Drawable* a, Drawable* b){return a->getZindex() < b->getZindex();});
        assume_sorted=true;
    }

    std::vector<Drawable*>::iterator iter, end;
    for(iter = drawable_list.begin(), end = drawable_list.end() ; iter != end; ++iter) {
        (*iter)->Draw();
    }

}

void Renderer::RegisterLight(DrawableLight *light) {
    light_list.insert(light);
    return;
}

void Renderer::UnregisterLight(DrawableLight *light) {
    if(light != NULL) {
        auto it = std::find(light_list.begin(), light_list.end(), light);
        if(it != light_list.end()) {
            std::cout << "DEBUG: found & erased light."<< std::endl;
            light_list.erase(it);
        } else {
            std::cout << "DEBUG: not found light."<< std::endl;
        }
    }
    return;
}

void Renderer::SetLightGradient(ALLEGRO_BITMAP *g) {
    light_gradient=g;
    lightmap=al_create_bitmap(al_get_display_width(display),al_get_display_height(display));
    return;
}

ALLEGRO_BITMAP *Renderer::GetLightGradient() const {
    return light_gradient;
}

void Renderer::RenderLightmap() {
    al_set_target_bitmap(lightmap); // draw on light map
    al_clear_to_color(al_map_rgb(255-GetBrightness(),255-GetBrightness(),255-GetBrightness())); //make lightmap entirely white, thus subtracting all light  //todo add value lightSetting to renderer, which is subtracted from 255 in the clear to color
    //al_clear_to_color(al_map_rgb(250,250,250)); // this will allow ~5% lighting with no light sources

    std::set<DrawableLight*>::iterator iter, end;
    for(iter = light_list.begin(), end = light_list.end() ; iter != end; ++iter) {
        (*iter)->Draw(); // light objects draw their gradients to the lightmap
    }

    al_set_target_backbuffer(display); // reset drawing to backbuffer

    al_set_blender(ALLEGRO_DEST_MINUS_SRC,ALLEGRO_ONE,ALLEGRO_ONE); // set subtractive blending
    al_draw_bitmap(lightmap,0,0,0);

}

Renderer::Renderer(ALLEGRO_DISPLAY *display) : display(display) {}

void Renderer::SaveState() {
    al_store_state(&blenderstate,ALLEGRO_STATE_BLENDER);
}

void Renderer::LoadState() {
    al_restore_state(&blenderstate);
}

void Renderer::ClearLights() {
    std::set<DrawableLight*> garbage;

    for(DrawableLight* l:light_list){
        if(l==NULL){
            garbage.insert(l);
        }
    }

    for(DrawableLight* l:garbage){
        light_list.erase(l);
    }
    return;
}

void Renderer::SetBrightness(int b) {
    if(b<=255) {
        brightness_modifier = b;
    }
}

int Renderer::GetBrightness() {
    return brightness_modifier;
}

