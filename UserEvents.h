//
// Created by Admin on 8/16/2017.
//

#ifndef BOX2DPLATFORM_USEREVENTS_H
#define BOX2DPLATFORM_USEREVENTS_H


#include <allegro5/events.h>
#include <Box2D.h>
#include "BaseEntity.h"
#include "BaseProjectile.h"

enum user_event_types{
    EVENT_DEBUG=20170,
    EVENT_DEBUG2,
    EVENT_BEGIN_COLLISION,
    EVENT_END_COLLISION,
    EVENT_GUI_ACTION,
    EVENT_PROJECTILE_HIT
};

enum GUI_action_types{
    GUI_ACTION_BUTTON,//todo add more
    GUI_ACTION_SLIDER,
    GUI_ACTION_TEXTBOX,
    GUI_ACTION_MISC
};

namespace UserEvents {

    extern ALLEGRO_EVENT_SOURCE user_events;

    struct CollisionEventData{
        b2Contact *contact;
    };

    struct ProjectileHitEventData{
        BaseEntity* hit;
        BaseProjectile* projectile;
    };

    struct GUIEventData{
        GUI_action_types type;
        void* data;
    };

    //static ALLEGRO_EVENT e;

    void ThrowDebugEvent(void* data);
    void ThrowCollisionBeginEvent(CollisionEventData* data);
    void ThrowCollisionEndEvent(CollisionEventData* data);

    void ThrowGUIActionEvent(GUIEventData* data);

    void ThrowProjectileHitEvent(ProjectileHitEventData* data);
};


#endif //BOX2DPLATFORM_USEREVENTS_H
