//
// Created by Admin on 8/19/2017.
//

#ifndef BOX2DPLATFORM_PROJECTILESMALLBULLET_H
#define BOX2DPLATFORM_PROJECTILESMALLBULLET_H


#include "BaseProjectile.h"
#include "Drawable.h"

class ProjectileSmallBullet : public BaseProjectile, public Drawable {
public:
    ProjectileSmallBullet(b2World *w,Renderer *renderer);
    virtual void Draw();

    virtual ~ProjectileSmallBullet();
};


#endif //BOX2DPLATFORM_PROJECTILESMALLBULLET_H
