//
// Created by Admin on 8/17/2017.
//

#ifndef BOX2DPLATFORM_BASEENTITYGARBAGEHANDLER_H
#define BOX2DPLATFORM_BASEENTITYGARBAGEHANDLER_H


#include <set>
#include "BaseEntity.h"

class BaseEntityGarbageHandler {
public:
    std::set<BaseEntity*> garbagePile; // anything entity to be deleted goes here

    void MarkForDelete(BaseEntity* be);

    void DeleteAllMarked();

};


#endif //BOX2DPLATFORM_BASEENTITYGARBAGEHANDLER_H
