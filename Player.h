//
// Created by Admin on 8/13/2017.
//

#ifndef BOX2DPLATFORM_PLAYER_H
#define BOX2DPLATFORM_PLAYER_H

#include <Box2D.h>
#include "BaseEntity.h"
#include "Drawable.h"

enum PLAYERDIRECTION{
    DIR_NONE,
    DIR_LEFT,
    DIR_RIGHT,
    DIR_UP,
    DIR_DOWN,
    DIR_FALLING
};

class Player : public BaseEntity, public Drawable {
    b2Fixture * bodyFixture;
    int feetTouching;
public:
    ALLEGRO_BITMAP* player_left;
    ALLEGRO_BITMAP* player_right;

    void SetLeftRightSprites(ALLEGRO_BITMAP* pl,ALLEGRO_BITMAP* pr);

//    //b2World *world; these are now defined by baseentity
//    int getFeetTouching() const;
//
//    void addFeetTouching();
//    void subtractFeetTouching();
//    //b2Body *body;

    PLAYERDIRECTION direction;

    Player(b2World *w,Renderer *renderer);

    bool canJump(); // check foot sensor
    void Jump(); // jump if possible

    virtual void Draw();

};


#endif //BOX2DPLATFORM_PLAYER_H
