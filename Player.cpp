//
// Created by Admin on 8/13/2017.
//



#include <Dynamics/b2World.h>
#include <allegro5/allegro.h>
#include <iostream>
#include "Player.h"
#include "UserEvents.h"
#include "GameObjectData.h"

Player::Player(b2World *w,Renderer *renderer) {
    world = w; // grab the world reference
    Register(renderer,Z_PLAYER); // register it w renderer with z index 1000
    direction=DIR_RIGHT;
    feetTouching=0;

    b2BodyDef playerBodyDef; // define starting parameters for player body
    playerBodyDef.type = b2_dynamicBody; // dynamic body
    playerBodyDef.position.Set(0, 0); //starting position
    playerBodyDef.angle = 0; //starting angle

    //set user data
    auto *god = new gameobjectdata;
    god->type = OBJECT_PLAYER;
    god->data = this;

    playerBodyDef.userData = god;

    playerBodyDef.fixedRotation = true;

    body=world->CreateBody(&playerBodyDef);

    b2PolygonShape boxShape;
    boxShape.SetAsBox(.5,.5); //  TAKES HALF-WIDTH AND HALF-HEIGHT, I WAS USING 1,1 WHICH MAKES A 2X2 BOX

    b2FixtureDef boxFixtureDef;
    boxFixtureDef.shape = &boxShape;
    boxFixtureDef.restitution = 0;
    boxFixtureDef.density = 1;
    boxFixtureDef.friction = 1;

    // ENABLE THE FOLLOWING FOR NO-COLLIDE WITH PLAYER
    boxFixtureDef.filter.groupIndex = FILTER_PLAYER;

    bodyFixture=body->CreateFixture(&boxFixtureDef);
    bodyFixture->SetUserData((void*)10);

    // i cant figure out foot sensors, the below is a head sensor. lets see if normals are easier */
/*    // here is the 'foot' sensor fixture that will tell us if our feet are on the ground
    //reusing the player's definitions since we are done with them
    boxShape.SetAsBox(0.3,0.3,b2Vec2(0,-1.5),0); // .6 * .6 sensor at 0, -0.5 from the center of my 1x1 character.  so the feet
    boxFixtureDef.isSensor=true; // make it a sensor
    footSensorFixture = body->CreateFixture(&boxFixtureDef);

    footSensorFixture->SetUserData((void*)12); // dunno if this will be needed later. lets see*/
}

void Player::Draw() {
    //do draw stuff
    if(body != nullptr) {
        b2Vec2 pos = body->GetPosition();
        float x = pos.x * PIX_METER;
        float y = pos.y * PIX_METER;

        // choose bmp based on direction
        if(direction==DIR_UP) {
        } else if(direction==DIR_DOWN) {
        } else if(direction==DIR_LEFT) {
            SetSprite(player_left);
        } else if(direction==DIR_RIGHT) {
            SetSprite(player_right);
        }

        al_draw_rotated_bitmap(current_sprite, half_w, half_h, x + half_w, y + half_h, body->GetAngle(), 0);
    }
}

void Player::SetLeftRightSprites(ALLEGRO_BITMAP *pl, ALLEGRO_BITMAP *pr) {
    player_left=pl;
    player_right=pr;
}

//int Player::getFeetTouching() const {
//    return feetTouching;
//}
//
//void Player::addFeetTouching() {
//    std::cout << "+ Feet touching!";
//    Player::feetTouching++;
//}
//
//void Player::subtractFeetTouching() {
//    std::cout << "- Feet touching!";
//    Player::feetTouching--;
//}


//todo error: reference to 'body' is ambiguous... so i need to actually distinguish between drawables body and phys ents body. and set drawable body = to phys body on creation