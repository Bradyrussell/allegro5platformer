//
// Created by Admin on 8/14/2017.
//

#ifndef BOX2DPLATFORM_LEVEL_H
#define BOX2DPLATFORM_LEVEL_H


#include <allegro5/config.h>
#include <Dynamics/b2World.h>
#include "StaticTerrainBlock.h"

class Level {
public:
    int level_map[40][30]; // 40 tiles wide, 30 tiles tall
    ALLEGRO_CONFIG* config;

    std::string name;
    float spawnx,spawny;

    int GetTile(int x, int y);
    void SetTile(int x, int y, int t);

    bool Load(const char* f);
    bool Save(const char* f);

    void Zero();

    void GenerateFromSet(std::set<StaticTerrainBlock*> *stbset);
    void Create(b2World *w, Renderer* r,std::set<StaticTerrainBlock*> *stbset);

    //std::set<Entity*> entities;


};


#endif //BOX2DPLATFORM_LEVEL_H
