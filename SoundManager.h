//
// Created by Admin on 8/17/2017.
//

#ifndef BOX2DPLATFORM_SOUNDMANAGER_H
#define BOX2DPLATFORM_SOUNDMANAGER_H

#include <vector>

#define DEFAULT_AUDIO_SAMPLES 128

class SoundManager {

public:
    int antiSpamCount;

    ALLEGRO_SAMPLE_ID *bgm_samp_id;

    bool Init(int samples=DEFAULT_AUDIO_SAMPLES);

    int percentVolumeModifier; // multiplied by passed gain

    float panModifier; // added to passed pan

    virtual ~SoundManager();

    void Play(ALLEGRO_SAMPLE* sample,float gain=1.0, float pan=0.0, float speed=1.0, ALLEGRO_PLAYMODE mode=ALLEGRO_PLAYMODE_ONCE);
    void PlayBGM(ALLEGRO_SAMPLE* sample,float gain=1.0, float pan=0.0, float speed=1.0, ALLEGRO_PLAYMODE mode=ALLEGRO_PLAYMODE_ONCE);

    void StopBGM();

    bool AntiSpamCanPlay();
    void AntiSpamTick();

    void SetVolumePercent(int percent);
    int GetVolumePercent();
    float GetVolumeDecimal();

    void SetPan(float p);
    float GetPan();

};


#endif //BOX2DPLATFORM_SOUNDMANAGER_H
