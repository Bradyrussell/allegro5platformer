//
// Created by Admin on 8/19/2017.
//

#include "ProjectileSmallBullet.h"
#include "GameObjectData.h"

ProjectileSmallBullet::ProjectileSmallBullet(b2World *w, Renderer *renderer) {
    world = w; // grab the world reference
    Register(renderer,Z_GENERICENTITY); // z index genericentity

    b2BodyDef canBodyDef; // define starting parameters for player body
    canBodyDef.type = b2_dynamicBody; // dynamic body
    canBodyDef.position.Set(0, 0); //starting position
    canBodyDef.angle = 0; //starting angle
    canBodyDef.bullet = true;

    canBodyDef.fixedRotation = false;

    //set user data
    auto *god = new gameobjectdata;
    god->type = OBJECT_PROJECTILESMALLBULLET;
    god->data = this;
    god->projectile = true;
    canBodyDef.userData = god;

    body=world->CreateBody(&canBodyDef);

    b2CircleShape canShape;
    canShape.m_p.Set(0,0);
    canShape.m_radius = .2;

    b2FixtureDef circleFixtureDef;
    circleFixtureDef.shape = &canShape;
    circleFixtureDef.restitution = .5;
    circleFixtureDef.density = 1;

    // ENABLE THE FOLLOWING FOR NO-COLLIDE WITH PLAYER
    circleFixtureDef.filter.groupIndex = FILTER_PLAYER;

    body->CreateFixture(&circleFixtureDef);
}

void ProjectileSmallBullet::Draw() {
    //do draw stuff
    if(body != nullptr) {
        b2Vec2 pos = body->GetPosition();

        float x = pos.x * PIX_METER;
        float y = pos.y * PIX_METER;
        al_draw_rotated_bitmap(current_sprite, half_w, half_h, x + half_w, y + half_h, body->GetAngle(), 0);

        if(!body->IsAwake()) {
            //std::cout << "Canister has come to rest." << std::endl;
        }
    }
}

ProjectileSmallBullet::~ProjectileSmallBullet() {

}
