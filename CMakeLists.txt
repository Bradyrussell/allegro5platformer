cmake_minimum_required(VERSION 3.7)
project(Box2DPlatform)

set(CMAKE_CXX_STANDARD 11)

SET(GCC_COVERAGE_COMPILE_FLAGS "-std=c++11 -O2")
add_definitions(${GCC_COVERAGE_COMPILE_FLAGS})

set(SOURCE_FILES main.cpp Player.cpp Player.h StaticTerrainBlock.cpp StaticTerrainBlock.h Level.cpp Level.h BaseEntity.cpp BaseEntity.h EntityBall.cpp EntityBall.h Assets.h Drawable.cpp Drawable.h Renderer.cpp Renderer.h ContactListener.cpp ContactListener.h DebugDraw.h BaseProjectile.cpp BaseProjectile.h UserEvents.cpp UserEvents.h GameObjectData.h ProjectileCanister.cpp ProjectileCanister.h SoundManager.cpp SoundManager.h DrawableLight.cpp DrawableLight.h BaseEntityGarbageHandler.cpp BaseEntityGarbageHandler.h GUIManager.cpp GUIManager.h TestMenu.cpp TestMenu.h ProjectileSmallBullet.cpp ProjectileSmallBullet.h BaseItem.cpp BaseItem.h)

LINK_DIRECTORIES(Box2DPlatform E:/Programming/C++/LIBRARIES/AllegroLIB/lib)
LINK_DIRECTORIES(Box2DPlatform E:/Programming/C++/LIBRARIES/box2d/lib)
LINK_DIRECTORIES(Box2DPlatform E:/Programming/C++/LIBRARIES/AGUI_built/lib)

add_executable(Box2DPlatform ${SOURCE_FILES})

INCLUDE_DIRECTORIES(Box2DPlatform E:/Programming/C++/LIBRARIES/AllegroLIB/include)
INCLUDE_DIRECTORIES(Box2DPlatform E:/Programming/C++/LIBRARIES/box2d/include)
INCLUDE_DIRECTORIES(Box2DPlatform E:/Programming/C++/LIBRARIES/AGUI_built/include)

TARGET_LINK_LIBRARIES(Box2DPlatform libagui.a libagui_allegro5.a libAMD.a libBox2ddebug.a)
