//
// Created by Admin on 8/18/2017.
//

#include "TestMenu.h"

TestMenu::TestMenu(agui::Gui *gui) {
    TestMenu::gui=gui;
    gui->add(&flow);

    //flow.

    flow.add(&text);
    text.setSize(140,20);
    text.setText("Hello world.");
    text.setMaxLength(12);
    RegisterWidget("text",&text);

    flow.add(&button);
    button.setSize(160,40);
    button.setText("Clear Entities");
    button.addActionListener(&listener);
    RegisterWidget("clear",&button);

    flow.add(&slider);
    slider.setSize(100,36);
    slider.setMaxValue(255);
    slider.setMarkerSize(agui::Dimension(10,30));
    slider.addActionListener(&listener);
    RegisterWidget("slider",&slider);

    flow.add(&button2);
    button2.setSize(20,20);
    button2.setText("X");
    button2.addActionListener(&listener);
    RegisterWidget("close",&button2);

}

void TestMenu::RegisterWidget(std::string id, agui::Widget *w) {
    if(w!=nullptr) {
        id_map.insert(std::pair<std::string, agui::Widget *>(id, w));
    }
}

agui::Widget *TestMenu::LookupWidget(std::string id) {
    agui::Widget *r;
    try {
        r=id_map.at(id);
    } catch(std::exception &e){
        return nullptr;
    }
    return r;
}

void TestMenu::UnregisterWidget(std::string id) {
    id_map.erase(id);
}
