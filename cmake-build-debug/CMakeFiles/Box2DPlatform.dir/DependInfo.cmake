# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "E:/Programming/C++/Box2DPlatform/BaseEntity.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/BaseEntity.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/BaseEntityGarbageHandler.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/BaseEntityGarbageHandler.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/BaseItem.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/BaseItem.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/BaseProjectile.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/BaseProjectile.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/ContactListener.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/ContactListener.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/Drawable.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/Drawable.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/DrawableLight.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/DrawableLight.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/EntityBall.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/EntityBall.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/GUIManager.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/GUIManager.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/Level.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/Level.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/Player.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/Player.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/ProjectileCanister.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/ProjectileCanister.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/ProjectileSmallBullet.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/ProjectileSmallBullet.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/Renderer.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/Renderer.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/SoundManager.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/SoundManager.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/StaticTerrainBlock.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/StaticTerrainBlock.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/TestMenu.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/TestMenu.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/UserEvents.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/UserEvents.cpp.obj"
  "E:/Programming/C++/Box2DPlatform/main.cpp" "E:/Programming/C++/Box2DPlatform/cmake-build-debug/CMakeFiles/Box2DPlatform.dir/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../Box2DPlatform"
  "E:/Programming/C++/LIBRARIES/AllegroLIB/include"
  "E:/Programming/C++/LIBRARIES/box2d/include"
  "E:/Programming/C++/LIBRARIES/AGUI_built/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
