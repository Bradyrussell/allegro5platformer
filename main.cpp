

//#include <allegro5/haptic.h>
#include <allegro5/timer.h>
#include <iostream>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/system.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <Dynamics/b2World.h>
#include <set>
#include <string>
#include <allegro5/allegro_audio.h>
#include <cstdio>
#include "Assets.h"
#include "Player.h"
#include "StaticTerrainBlock.h"
#include "Level.h"
#include "EntityBall.h"
#include "ContactListener.h"
#include "DebugDraw.h"
#include "UserEvents.h"
#include "GameObjectData.h"
#include "ProjectileCanister.h"
#include "SoundManager.h"
#include "BaseEntityGarbageHandler.h"
#include "GUIManager.h"
#include "TestMenu.h"
#include "ProjectileSmallBullet.h"
#include "SoundSettingsMenu.h"
#include "Utilities.h"

#define SCREENWIDTH 640
#define SCREENHEIGHT 480
#define FPS_LIMIT 60
#define PIX_METER 16;
#define MULTISAMPLE_SAMPLES 8

#define TITLE "Platformer 0.0.2"

ALLEGRO_DISPLAY *display;
ALLEGRO_TIMER *timer;
ALLEGRO_EVENT_QUEUE *events;

GUIManager GUI;
SoundManager SOUND;

//TestMenu *testMenu;

//todo make a header file with all defines (fix collision of SCREENHEIGHT & width)
//todo (dunno if this is feasable) multithreading so that box2d physics doesnt kill window responsiveness. i assume box2d and rendering would have to be same thread. but mayne event Q or callbacks?
// when the level is repeatedly loaded  some weeeeird shit happens.   entities sometimes will draw tile textures behind them. tiles or their textures will change to other tiles/textures

//todo   ----> create a entity map with ids similar to the widget map in base menus <----

//todo make lights not entirely 255, so that two lights will be brighter than 1


namespace init{
    bool Begin(){
        std::cout << "Begin " << TITLE << std::endl;

        if(al_init() && al_init_image_addon() && al_install_keyboard() && al_install_mouse() && al_init_primitives_addon() && al_install_audio() && al_init_font_addon() && al_init_ttf_addon()) {
            //hopefully this sets anti-aliasing
            al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_REQUIRE);
            al_set_new_display_option(ALLEGRO_SAMPLES, MULTISAMPLE_SAMPLES, ALLEGRO_SUGGEST);

            //more graphics upgrades
            /* "When drawing a scaled down version of the bitmap, use linear filtering. This usually looks better. You can also combine it with the MIPMAP flag for even better quality.' */
            /* https://www.allegro.cc/manual/5/al_set_new_bitmap_flags */
            al_set_new_bitmap_flags(ALLEGRO_MIN_LINEAR | ALLEGRO_MIPMAP);

            //create window
            display = al_create_display(SCREENWIDTH, SCREENHEIGHT);
            al_set_window_title(display, TITLE); //set window title

            std::cout << "Drawing loading screen..." << std::endl;

            assets::Preload(display,SCREENWIDTH,SCREENHEIGHT); // draw loading screen

            if(!SOUND.Init()){
                return false;
            }

            timer = al_create_timer(1.0 / FPS_LIMIT); // create game loop timer
            events = al_create_event_queue(); //create event queue

            //register event sources
            al_register_event_source(events,al_get_mouse_event_source());
            al_register_event_source(events,al_get_keyboard_event_source());
            al_register_event_source(events, al_get_display_event_source(display));
            al_register_event_source(events, al_get_timer_event_source(timer));

            //register user event source
            al_init_user_event_source(&UserEvents::user_events); // right now userevents is static might not work
            al_register_event_source(events,&UserEvents::user_events); // duhhh i forgot this for the longest time

            GUI.Init();

            //load assets
            if(!assets::Load()){
                //error loading assets, probably cant find one
                std::cout << "Error loading assets! Most likely one is missing." << std::endl; // one of the inits failed, cant run
                return false;
            } //else { return false; } // mozilla coding style says this is bad...
            return true;
        }
        return false;
    }


}

int main() {
    if(init::Begin()) { //init allegro and subsystems we need
        //all inits good

        // init box2d stuff here
        b2World world(b2Vec2(0.0f,9.0f)); // gravity x , gravity y

        // attach debug drawing class here
        DebugDraw debug_draw;
        world.SetDebugDraw(&debug_draw);

        Renderer renderer(display); //error unable to create variable object
        renderer.SetLightGradient(assets::gradient_bmp); // lightmap still null after this

        // init game objects here
        Player theplayer(&world,&renderer);
        theplayer.SetLeftRightSprites(assets::beep_left,assets::beep_right);

        DrawableLight playerlight(&renderer);
        playerlight.setIntensity(1);

        ContactListener contactListener(&theplayer);
        world.SetContactListener(&contactListener);

        bool move_up=false,move_left=false,move_right=false,move_down=false;

        std::set<StaticTerrainBlock*> terrainset;
        BaseEntityGarbageHandler entity_trash;

        al_start_timer(timer); // start game loop timer

        al_rest(1.0); //debug delay

        std::cout << "End init, starting game loop." << std::endl; // one of the inits failed, cant run

        /* END OF INITIALIZATION */

        /* BEGIN GAME LOOP */
        bool redraw = true;

        SOUND.PlayBGM(assets::bgm1_smp,3.0,0,1,ALLEGRO_PLAYMODE_LOOP);

        //int antiNoiseSpamTimer=0;

        while(true){
            ALLEGRO_EVENT ev;
            al_wait_for_event(events, &ev); // block until we get an event

            // handle gui events
            GUI.HandleInput(ev);

            if(ev.type == ALLEGRO_EVENT_TIMER) { // draw event happens @ 60 FPS / Hz
                SOUND.AntiSpamTick();
                entity_trash.DeleteAllMarked();
                // do physics here
                world.Step(1.0f / FPS_LIMIT,10,10); // step the world using a constant 60 Hz with 10 velocity & position iterations
                world.ClearForces();

                redraw = true;
            }
            else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) { // user closes the window
                goto ENDGAME;
                break;
            }
            else if(ev.type == ALLEGRO_EVENT_KEY_DOWN) {
                if(!GUI.isUsingKeys()) {
                    switch (ev.keyboard.keycode) { // switch based on what key was pressed
                        case (ALLEGRO_KEY_E): {
                            theplayer.body->ApplyAngularImpulse(10, 1);
                            break;
                        }
                        case (ALLEGRO_KEY_Q): {
                            theplayer.body->ApplyAngularImpulse(-10, 1);
                            break;
                        }
                        case (ALLEGRO_KEY_UP): {
                            move_up = true;
                            //theplayer.body->ApplyLinearImpulse(b2Vec2(0, -(theplayer.body->GetMass()*5)), theplayer.body->GetWorldCenter(), 1);
                            break;
                        }
                        case (ALLEGRO_KEY_DOWN): {
                            move_down = true;
                            break;
                        }
                        case (ALLEGRO_KEY_LEFT): {
                            move_left = true;
                            break;
                        }
                        case (ALLEGRO_KEY_RIGHT): {
                            move_right = true;
                            break;
                        }
                        case (ALLEGRO_KEY_A): {
                            move_left = true;
                            break;
                        }
                        case (ALLEGRO_KEY_D): {
                            move_right = true;
                            break;
                        }
                        case (ALLEGRO_KEY_SPACE): {
                            if(theplayer.body->GetLinearVelocity().y == 0) {
                                theplayer.body->ApplyLinearImpulseToCenter(b2Vec2(0, -(theplayer.body->GetMass() * 7)), 1);
                                SOUND.Play(assets::jump_smp);
                            }
                            break;
                        }
                        case (ALLEGRO_KEY_P): {
                            //testMenu = new TestMenu(GUI.gui);
                            auto *basemenu = new SoundSettingsMenu(GUI.gui);
                            GUI.CreateMenu(basemenu,"debugmenu");
                            GUI.setUsingMouseKeys(true);
                            renderer.SetBrightness(200);
                            break;
                        }
//                        case (ALLEGRO_KEY_O): {
//                            GUI.Clear();
//                            //GUI.setUsingMouse(false); //now in gui.clear
//                            break;
//                        }
                        case (ALLEGRO_KEY_PGDN): {
//                            std::cout << "Begin save level file!" << std::endl;
//                            Level newlev;
//                            newlev.GenerateFromSet(&terrainset);
//                            newlev.Save("LEVEL.LVL");
//                            std::cout << "End save level file!" << std::endl;
                            break;
                        }
                        case (ALLEGRO_KEY_L): {
                            for (StaticTerrainBlock *cstb:terrainset) {
                                delete cstb;
                            }
                            terrainset.clear();
                            std::cout << "Creating level from file!" << std::endl;
                            Level loadlev;
                            loadlev.Load("LEVEL.LVL");
                            loadlev.Create(&world, &renderer, &terrainset);

                            std::cout << "Loaded level \'" << loadlev.name << "\'!" << std::endl;

                            theplayer.body->SetTransform(b2Vec2(loadlev.spawnx, loadlev.spawny),
                                                         0); // move the character to the spawnpoint
                            theplayer.body->SetLinearVelocity(b2Vec2(0, 0));

                            for (StaticTerrainBlock *cstb:terrainset) {
                                switch (cstb->type) {
                                    case (0): {
                                        cstb->SetSprite(assets::tile0_bmp);
                                        break;
                                    }
                                    case (1): {
                                        cstb->SetSprite(assets::tile1_bmp);
                                        break;
                                    }
                                    case (2): {
                                        cstb->SetSprite(assets::tile0_bmp);
                                        break;
                                    }
                                    case (3): {
                                        cstb->SetSprite(assets::tile3_bmp);
                                        break;
                                    }
                                    default: {
                                    }
                                }
                            }


                            break;
                        }
                        case (ALLEGRO_KEY_ENTER): {
                            std::cout << "Player at: " << (int) theplayer.body->GetPosition().x << ","
                                      << (int) theplayer.body->GetPosition().y << std::endl;
                            theplayer.body->SetTransform(b2Vec2(0,0),0);
                            theplayer.body->SetLinearVelocity(b2Vec2(0,0));
                            break;
                        }
                        case (ALLEGRO_KEY_F1): { // doing this 6 times  at once crashes game
                            SOUND.SetVolumePercent(0);
                            SOUND.StopBGM(); /// stop bgm doesnt work
                            for(int i =0; i<25; i++) {
                                auto *p = new ProjectileCanister(&world, &renderer);
                                p->SetSprite(assets::can_bmp);
                                float px = i+1;
                                float py = 1;
                                p->SetPosition(b2Vec2(px,py));
                            }
                            break;
                        }
                        case (ALLEGRO_KEY_ESCAPE): {
                            goto ENDGAME;
                            break;
                        }
                        default:
                            break;
                    }
                }

            }
            else if(ev.type == ALLEGRO_EVENT_KEY_UP) {
                if(!GUI.isUsingKeys()) {
                    switch (ev.keyboard.keycode) {
                        case (ALLEGRO_KEY_UP): {
                            move_up = false;
                            break;
                        }
                        case (ALLEGRO_KEY_DOWN): {
                            move_down = false;
                            break;
                        }
                        case (ALLEGRO_KEY_LEFT): {
                            move_left = false;
                            break;
                        }
                        case (ALLEGRO_KEY_RIGHT): {
                            move_right = false;
                            break;
                        }
                        case (ALLEGRO_KEY_A): {
                            move_left = false;
                            break;
                        }
                        case (ALLEGRO_KEY_D): {
                            move_right = false;
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                }
            }
            else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {// user clicks mouse button
                if(!GUI.isUsingMouse()) { // make sure GUI isnt using the mouse
                    if (ev.mouse.button == 1) { // left click

                        auto *p = new ProjectileSmallBullet(&world, &renderer);
                        p->SetSprite(assets::smallbullet_bmp);
                        //entityset.insert((BaseEntity *) p);
                        float px = ev.mouse.x / PIX_METER;
                        float py = ev.mouse.y / PIX_METER;

                        p->SetPosition(theplayer.GetPosition());   // todo create small flash of light on shoot
                        //p->Launch(b2Vec2(3,0));  // works
                        p->setShooter(&theplayer);
                        p->setBullet(true);
                        p->LaunchTowardsWithForce(b2Vec2(px, py),10); // shoot from player to mouse
                        SOUND.Play(assets::shot_smp,.6);

                    } else if (ev.mouse.button == 3) { // middle click
                        for (int i = 0; i < 5; i++) {
                            auto *b = new EntityBall(&world, &renderer);
                            b->SetSprite(assets::ball_bmp);
                            //entityset.insert((BaseEntity *) b);
                            float px = ev.mouse.x / PIX_METER;
                            float py = ev.mouse.y / PIX_METER;
                            b->SetPosition(px, py + 1);
                            float forcex = rand() % 3 - 1;
                            float forcey = rand() % 3 - 1;
                            b->body->ApplyLinearImpulseToCenter(b2Vec2(forcex * 10, forcey * 10), true);
                        }
                    } else if (ev.mouse.button == 2) { // right click
                        auto *p = new ProjectileCanister(&world, &renderer);
                        p->SetSprite(assets::can_bmp);
                        //entityset.insert((BaseEntity *) p);
                        float px = ev.mouse.x / PIX_METER;
                        float py = ev.mouse.y / PIX_METER;
                        p->SetPosition(theplayer.GetPosition());
                        //p->Launch(b2Vec2(3,0));  // works
                        p->LaunchTowards(b2Vec2(px, py)); // shoot from player to mouse

                        SOUND.Play(assets::debug_smp,.6);

                    }
                }
            } else if(ev.type==EVENT_DEBUG) {
                std::cout << "debugevent: " << ev.user.data1 << std::endl;
                al_unref_user_event((ALLEGRO_USER_EVENT*)&ev);
            }else if((ev.type==EVENT_BEGIN_COLLISION) && ((void*)ev.user.data1 != nullptr)) {

                auto *d = (UserEvents::CollisionEventData*)ev.user.data1;
                auto *godA = (gameobjectdata*)d->contact->GetFixtureA()->GetBody()->GetUserData();
                auto *godB = (gameobjectdata*)d->contact->GetFixtureB()->GetBody()->GetUserData();

                if(godA != nullptr && godB != nullptr) {

                    if (godA->type == OBJECT_ENTITYBALL) {
                        auto *ball = (EntityBall *) godA->data;
                        if (ball != nullptr) {
                            //entity_trash.MarkForDelete(ball);
                        }
                        //b2Vec2 pos = ((EntityBall*)godA->data)->GetPosition();
                        //DrawableLight *newlight = new DrawableLight(&renderer,pos,1);
                        //SOUND.Play(assets::clang_smp);
                    } else if (godB->type == OBJECT_ENTITYBALL) {
                        auto *ball = (EntityBall *) godB->data;
                        if (ball != nullptr) {
                            //entity_trash.MarkForDelete(ball);
                        }
                        //SOUND.Play(assets::clang_smp);
                        //((EntityBall*)godB->data)->body->SetGravityScale(0);
                    }

//                    if (godB->type == OBJECT_PROJECTILECANISTER && godA->type == OBJECT_PROJECTILECANISTER) {
//                        auto* A = (BaseEntity*)godA->data;
//                        auto* B = (BaseEntity*)godB->data;
//                        entity_trash.MarkForDelete(A);
//                        entity_trash.MarkForDelete(B);
//                    }

                    if (godA->type == OBJECT_PROJECTILECANISTER) {
                        auto *objcan = (ProjectileCanister *) godA->data;
                        objcan->Bounce();
                        if (SOUND.AntiSpamCanPlay()) {
                            SOUND.Play(assets::clang_smp, 0.3);
                        }
                        //entityset.erase((BaseEntity*)godA->data);
                        //((ProjectileCanister*)godA->data)->Destroy();
                    } else if (godB->type == OBJECT_PROJECTILECANISTER) {
                        auto *objcan = (ProjectileCanister *) godA->data;
                        objcan->Bounce();
                        if (SOUND.AntiSpamCanPlay()) {
                            SOUND.Play(assets::clang_smp, 0.3);
                        }
                        //entityset.erase((BaseEntity*)godB->data);
                        //((ProjectileCanister*)godB->data)->Destroy();
                    }


                }

                al_unref_user_event((ALLEGRO_USER_EVENT*)&ev);
                //delete &ev;
            } else if((ev.type==EVENT_PROJECTILE_HIT) && ((void*)ev.user.data1 != nullptr)) {

                auto *d = (UserEvents::ProjectileHitEventData*)ev.user.data1;
                auto godProjectile = (gameobjectdata*)d->projectile->body->GetUserData();

                d->projectile->Bounce();

                if(godProjectile->type==OBJECT_PROJECTILECANISTER) {
                    if (SOUND.AntiSpamCanPlay()) {
                        SOUND.Play(assets::clang_smp, 0.3);
                    }
                    if (d->projectile->bouncecounter > 4) {
                        //explode
                        //d->projectile->world->QueryAABB() //todo create an Explode(b2vec2 pos, float force) function. probably in a 'world' class
                        for (b2Body* b = world.GetBodyList(); b; b = b->GetNext()) { // this shitty explode needs replaced //todo create particle system: duration of existence, movement etc
                            if(b2Distance(b->GetPosition(),d->projectile->body->GetPosition()) < 5){
                                b2Vec2 me = d->projectile->body->GetPosition();
                                b2Vec2 dif = b->GetPosition() - me;
                                b->ApplyLinearImpulseToCenter(b2Vec2(dif.x*10,dif.y*10),true);

                                float rot = (rand()%2) - 1; // do randon rotation -9 to +9
                                b->ApplyAngularImpulse(rot*(rand()%10),true);
                                SOUND.Play(assets::boom_smp,.2);
                            }
                        }
                        entity_trash.MarkForDelete(d->projectile);
                    }
                }

                if(godProjectile->type==OBJECT_PROJECTILESMALLBULLET) {
                   if(d->projectile->bouncecounter>1){
                        entity_trash.MarkForDelete(d->projectile);
                    }

                    if(d->hit != nullptr) { // hit an entity
                        auto godHit = (gameobjectdata*)d->hit->body->GetUserData();
                        SOUND.Play(assets::break_smp,.3);
                        if(godHit->type==OBJECT_ENTITYBALL){
                            entity_trash.MarkForDelete(d->hit);
                            entity_trash.MarkForDelete(d->projectile);
                        }

                        if(godHit->type==OBJECT_PROJECTILECANISTER){
                            ((ProjectileCanister*)godHit->data)->Bounce();
                            entity_trash.MarkForDelete(d->projectile);
                        }
                    }
                }
//todo //////////////////////////////GUI ACTION HERE/////////////////////////////////////////
            } else if(ev.type==EVENT_GUI_ACTION){ // GUI user event
                auto data = (UserEvents::GUIEventData*)(ev.user.data1);

                auto source_widget = (agui::Widget*)data->data;

                for(std::string menu_id:GUI.GetMenuList()) { // loop thru all current menus
                    BaseMenu* current_menu = GUI.GetMenu(menu_id);

                    std::string widget_id = current_menu->ReverseLookupWidget(source_widget); // attempt to find the widget that caused event inside given menu
                    if(!widget_id.empty()){ // we have found the correct menu, with the widget causing the event
                        if(menu_id == "debugmenu") {
                            agui::Slider* thevolslider = (agui::Slider*)current_menu->LookupWidget("slider");
                            int current_volumeslider_value = thevolslider->getValue();

                            if(widget_id == "stopsound") {
                                theplayer.body->ApplyLinearImpulseToCenter(b2Vec2(0,-10),true);
                            } else if(widget_id == "close") {
                                GUI.RemoveMenu(menu_id);
                                GUI.setUsingMouseKeys(false);
                            } else if(widget_id == "slider") {
                                char buf[16];
                                itoa(current_volumeslider_value,buf,10);
                                current_menu->LookupWidget("label")->setText(buf);
                            } else if(widget_id == "apply"){
                                SOUND.SetVolumePercent(current_volumeslider_value);
                                printf("SOUND SETTINGS SAVED: %i",current_volumeslider_value);
                            }
                        } else { // we cant find what menu this widget belongs to
                            util::msg::Message(widget_id.c_str(),"Stray Widget");
                        }


                    } // if we reach here, we did not find the right menu
                } // end of looping thru all menus
/*                auto s = (agui::Slider*)GUI.GetMenu("debugmenu")->LookupWidget("slider");
                float sd = s->getValue();

//                std::stringstream out; apparently string streams are sloooow and add tons to the exe
//                out << sd/2.55 << "%";
                char buf[16];
                std::sprintf(buf, "%f %%", (sd/2.55));
                GUI.GetMenu("debugmenu")->LookupWidget("text")->setText(buf);  // <---- you stopped here
                renderer.SetBrightness(sd);
                                                                                                        //  you were planning on making a BaseMenu class with essentially TestMenu                 // that all other menus will inherit. register these basemenus with the GUI handler
                //check what kind of gui action
                if(data->type==GUI_ACTION_BUTTON) { // we have determined it is a button action
                    auto button = (agui::Button*)(data->data);

                    if(button==GUI.GetMenu("debugmenu")->LookupWidget("close")) { //works
                        GUI.setUsingMouseKeys(false);
                        GUI.Clear();
                    }

                    if(button==GUI.GetMenu("debugmenu")->LookupWidget("clear")) {
                        for (b2Body* b = world.GetBodyList(); b; b = b->GetNext()) {
                            auto *game_obj_dat = (gameobjectdata*)b->GetUserData();

                            if(game_obj_dat->type==OBJECT_ENTITYBALL){
                                delete ((BaseEntity*)game_obj_dat->data);
                            }

                            if(game_obj_dat->type==OBJECT_PROJECTILECANISTER){
                                delete ((BaseEntity*)game_obj_dat->data);
                            }
                        }
                    }
                }
                delete data;*/
            }
//todo//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define MAX_SPEED 8

            // handle player movement & decision logic
            if(move_up){
                //theplayer.direction=DIR_UP; // i think it looks better if it only shows dir up when key is down
                theplayer.body->ApplyLinearImpulseToCenter(b2Vec2(0, -(theplayer.body->GetMass())/2), 1);
            }
            if(move_down){
                //theplayer.direction=DIR_DOWN;
                theplayer.body->ApplyLinearImpulse(b2Vec2(0, (theplayer.body->GetMass())), theplayer.body->GetWorldCenter(), 1);
            }
            if(move_left){
                theplayer.direction=DIR_LEFT;
                if(fabs(theplayer.body->GetLinearVelocity().x)<=MAX_SPEED) {
                    theplayer.body->ApplyLinearImpulse(b2Vec2(-(theplayer.body->GetMass() * .5), 0),
                                                       theplayer.body->GetWorldCenter(), 1);
                }
            }
            if(move_right){
                theplayer.direction=DIR_RIGHT;
                if(fabs(theplayer.body->GetLinearVelocity().x)<=MAX_SPEED) {
                    theplayer.body->ApplyLinearImpulse(b2Vec2((theplayer.body->GetMass() * .5), 0),
                                                       theplayer.body->GetWorldCenter(), 1);
                }
            }

            playerlight.setPosition(theplayer.GetPosition());

            if(redraw && al_is_event_queue_empty(events)) { // is it time to draw?
                redraw = false;

                GUI.Update();

                // DO DRAW
                al_clear_to_color(al_map_rgb(0,0,0)); // clear screen

                al_draw_bitmap(assets::bg_bmp,0,0,0); // draw bg

                renderer.SaveState();

                renderer.RenderFrame();
                renderer.RenderLightmap();

                renderer.LoadState();

                GUI.Draw();

                al_flip_display();
                //END DRAW
            }


        }

        /* END GAME LOOP */

    } else {
        std::cout << "One of the allegro init functions failed!" << std::endl; // one of the inits failed, cant run
        return 2;
    }

    ENDGAME:

    std::cout << "End " << TITLE << std::endl;
    return 0;
}