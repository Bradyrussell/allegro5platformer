//
// Created by Admin on 8/16/2017.
//

#ifndef BOX2DPLATFORM_DEBUGDRAW_H
#define BOX2DPLATFORM_DEBUGDRAW_H


#include <Common/b2Draw.h>

class DebugDraw : public b2Draw{
public:
    void DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color) override {

    }

    void DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color) override {

    }

    void DrawCircle(const b2Vec2 &center, float32 radius, const b2Color &color) override {

    }

    void DrawSolidCircle(const b2Vec2 &center, float32 radius, const b2Vec2 &axis, const b2Color &color) override {

    }

    void DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color) override {

    }

    void DrawTransform(const b2Transform &xf) override {

    }

    void DrawPoint(const b2Vec2 &p, float32 size, const b2Color &color) override {

    }
};


#endif //BOX2DPLATFORM_DEBUGDRAW_H
