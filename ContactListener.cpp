//
// Created by Admin on 8/16/2017.
//

#include <allegro5/allegro_primitives.h>
#include <iostream>
#include "ContactListener.h"
#include "UserEvents.h"
#include "BaseProjectile.h"
#include "GameObjectData.h"


ContactListener::ContactListener(Player *p){
    ContactListener::p = p;
}

void ContactListener::BeginContact(b2Contact* contact) {
    //check if fixture A was the foot sensor
    //void* fixtureUserData = contact->GetFixtureA()->GetUserData();
    //if ( (int)fixtureUserData == 10 ) {
        //p->addFeetTouching();
    //    UserEvents::ThrowDebugEvent((void*)1337);
    //}
    //check if fixture B was the foot sensor
    //fixtureUserData = contact->GetFixtureB()->GetUserData();
    //if ( (int)fixtureUserData == 10 ) {
    //    //p->addFeetTouching();
    //    UserEvents::ThrowDebugEvent((void*)1337);
    //}
    auto *godA = (gameobjectdata*)contact->GetFixtureA()->GetBody()->GetUserData();
    auto *godB = (gameobjectdata*)contact->GetFixtureB()->GetBody()->GetUserData();

    if(godA->projectile){
        //throw projectile event
        auto projd = new UserEvents::ProjectileHitEventData;
        projd->projectile = (BaseProjectile*)godA->data;
        if(godB->type!=OBJECT_TILE) {
            projd->hit = (BaseEntity *) godB->data;
        } else {
            projd->hit = nullptr;
        }
        UserEvents::ThrowProjectileHitEvent(projd);
        return; // prevent further event throwing .    maybe change this
    }

    if(godB->projectile){
        //std::cout << "Projectile hit event!" << std::endl;//throw projectile event
        auto projd = new UserEvents::ProjectileHitEventData;
        projd->projectile = (BaseProjectile*)godB->data;
        if(godA->type!=OBJECT_TILE) {
            projd->hit = (BaseEntity *) godA->data;
        } else {
            projd->hit = nullptr;
        }
        UserEvents::ThrowProjectileHitEvent(projd);
        return; // prevent further event throwing .    maybe change this
    }

    auto *d = new UserEvents::CollisionEventData;
    d->contact = contact;
    UserEvents::ThrowCollisionBeginEvent(d);
}

void ContactListener::EndContact(b2Contact* contact) {
//    //check if fixture A was the foot sensor
//    void* fixtureUserData = contact->GetFixtureA()->GetUserData();
//    if ( (int)fixtureUserData == 10 ) {
//        //p->subtractFeetTouching();
//        UserEvents::ThrowDebugEvent((void*)1337);
//    }
//    //check if fixture B was the foot sensor
//    fixtureUserData = contact->GetFixtureB()->GetUserData();
//    if ( (int)fixtureUserData == 10 ) {
//        //p->subtractFeetTouching();
//        UserEvents::ThrowDebugEvent((void*)1337);
//    }
    auto *d = new UserEvents::CollisionEventData;
    d->contact = contact;
    UserEvents::ThrowCollisionEndEvent(d);
}
