//
// Created by Admin on 8/18/2017.
//

#ifndef BOX2DPLATFORM_TESTMENU_H
#define BOX2DPLATFORM_TESTMENU_H


#include "GUIManager.h"

class TestMenu {
public:
    TestMenu(agui::Gui *gui);

    std::map<std::string,agui::Widget*> id_map;   //todo make base class that does all this menu stuff

    agui::Widget* LookupWidget(std::string id);
    void  UnregisterWidget(std::string id);


private:

    void RegisterWidget(std::string id,agui::Widget* w);

    MenuMiscListener listener;
    agui::Gui* gui;

    agui::FlowLayout flow;
    agui::Button button;

    agui::Button button2;

    agui::TextField text;

    agui::Slider slider;

};




#endif //BOX2DPLATFORM_TESTMENU_H
