//
// Created by Admin on 8/16/2017.
//

#ifndef BOX2DPLATFORM_BASEPROJECTILE_H
#define BOX2DPLATFORM_BASEPROJECTILE_H


#include <Common/b2Math.h>
#include "BaseEntity.h"

class BaseProjectile : public BaseEntity{
public:
    int bouncecounter;

    BaseEntity* shooter=nullptr; // change to living entity when i make that

    BaseEntity *getShooter() const;
    void setShooter(BaseEntity *shooter);

    void setBullet(bool b);

    void Launch(b2Vec2 v);
    void LaunchTowards(b2Vec2 v);
    void LaunchTowardsWithForce(b2Vec2 v, float force); // fail

    virtual void Bounce();

    BaseProjectile();

    virtual ~BaseProjectile();
};


#endif //BOX2DPLATFORM_BASEPROJECTILE_H
