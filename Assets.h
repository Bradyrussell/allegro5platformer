//
// Created by Admin on 8/14/2017.
//

#ifndef BOX2DPLATFORM_ASSETS_H
#define BOX2DPLATFORM_ASSETS_H

#include <allegro5/bitmap.h>
#include <allegro5/allegro_font.h>

namespace assets{ //

    ALLEGRO_BITMAP* preload_bmp;
    ALLEGRO_BITMAP* icon_bmp;

    ALLEGRO_BITMAP* debug_bmp;
    ALLEGRO_BITMAP* gun_bmp;

    ALLEGRO_BITMAP* tile0_bmp;
    ALLEGRO_BITMAP* tile1_bmp;
    ALLEGRO_BITMAP* tile2_bmp;
    ALLEGRO_BITMAP* tile3_bmp;

    ALLEGRO_BITMAP* beep_right;
    ALLEGRO_BITMAP* beep_left;
    ALLEGRO_BITMAP* beep_front;

    ALLEGRO_BITMAP* ball_bmp;

    ALLEGRO_BITMAP* smallbullet_bmp;
    ALLEGRO_BITMAP* largebullet_bmp;

    ALLEGRO_BITMAP* explosion_bmp;

    ALLEGRO_BITMAP* can_bmp;

    ALLEGRO_BITMAP* bg_bmp;

    ALLEGRO_BITMAP* gradient_bmp;

    ALLEGRO_SAMPLE* debug_smp;
    ALLEGRO_SAMPLE* jump_smp;
    ALLEGRO_SAMPLE* clang_smp;
    ALLEGRO_SAMPLE* shot_smp;
    ALLEGRO_SAMPLE* break_smp;

    ALLEGRO_SAMPLE* bgm1_smp;
    ALLEGRO_SAMPLE* boom_smp;

    ALLEGRO_FONT *font;

    bool Load() {
        assets::debug_bmp = al_load_bitmap("image/debug.png");
        assets::gun_bmp = al_load_bitmap("image/pistol.png");

        assets::tile0_bmp = al_load_bitmap("image/tile1.png");
        assets::tile1_bmp = al_load_bitmap("image/tile-grass.png");
        assets::tile2_bmp = al_load_bitmap("image/tile-glass.png");
        assets::tile3_bmp = al_load_bitmap("image/tile-dirt.png");

        assets::beep_right = al_load_bitmap("image/beep_right.png");
        assets::beep_left = al_load_bitmap("image/beep_left.png");
        assets::beep_front = al_load_bitmap("image/beep_front.png");

        assets::bg_bmp = al_load_bitmap("image/bg2.png");

        assets::can_bmp = al_load_bitmap("image/canister.png");

        assets::ball_bmp = al_load_bitmap("image/ball.png");

        assets::smallbullet_bmp = al_load_bitmap("image/smallbullet.png");
        assets::largebullet_bmp = al_load_bitmap("image/largebullet.png");

        assets::explosion_bmp = al_load_bitmap("image/explosion1.png");

        assets::gradient_bmp = al_load_bitmap("image/lightgradient.png");

        assets::debug_smp = al_load_sample("sound/click.wav");
        assets::jump_smp = al_load_sample("sound/boop.wav");
        assets::clang_smp = al_load_sample("sound/clang.wav");
        assets::shot_smp = al_load_sample("sound/gunshot2.wav");
        assets::break_smp = al_load_sample("sound/shatter1.wav");
        assets::boom_smp = al_load_sample("sound/explode1.wav");

        assets::bgm1_smp = al_load_sample("sound/bgwav.wav");

        assets::font = al_create_builtin_font();

        return (assets::debug_bmp != NULL) && (assets::gun_bmp != NULL); //todo check all assets for validity
    }

    void Preload(ALLEGRO_DISPLAY * d, int SCREENWIDTH, int SCREENHEIGHT) {// load loading screen and draw
        assets::preload_bmp = al_load_bitmap("image/loading.png");
        assets::icon_bmp = al_load_bitmap("image/beep_right.png");
        if((assets::preload_bmp != NULL) && (assets::icon_bmp != NULL)) {
            al_draw_scaled_bitmap(assets::preload_bmp, 0, 0, al_get_bitmap_width(assets::preload_bmp),
                                  al_get_bitmap_height(assets::preload_bmp), 0, 0, SCREENWIDTH, SCREENHEIGHT,
                                  0); // draw preload bmp
            al_set_display_icon(d, assets::icon_bmp);//set icon for window
            al_flip_display();
        }
    }


};

#endif //BOX2DPLATFORM_ASSETS_H
