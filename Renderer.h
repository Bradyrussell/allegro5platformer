//
// Created by Admin on 8/15/2017.
//

#ifndef BOX2DPLATFORM_RENDERER_H
#define BOX2DPLATFORM_RENDERER_H

#include <vector>
#include "Drawable.h"
#include "DrawableLight.h"

// ugh this might fix some retarded compiler whining
class Drawable;
class DrawableLight; // hey it works so...

//8/16/17 7:35   -  in order to implement Z ordering i changed from std::set to std::vector
// so as of 8/16/17 8:00 pm    Z order integer is sorted so that a higher number means it is rendered last, thus IN THE FOREGROUND ON TOP OF EVERYTHING
// High number =  foreground
// low number = background

//todo : maybe have insertions be already sorted, like insert at iterator. that way full re-ordering is rarely needed

#define SCREENWIDTH 640
#define SCREENHEIGHT 480

enum ZINDEX{
    Z_PLAYER=1000,
    Z_BACKGROUND=-1000,
    Z_GENERICENTITY=100,
    Z_LEVELTILES=10
};


class Renderer {
    bool assume_sorted=false;

    ALLEGRO_DISPLAY* display;
public:
    Renderer(ALLEGRO_DISPLAY *display);

private:

    ALLEGRO_BITMAP* light_gradient;
    ALLEGRO_BITMAP* lightmap;

    ALLEGRO_STATE blenderstate;

    int brightness_modifier=0;
public:
    std::vector<Drawable*> drawable_list; // todo make sure i handle deletion of bodies, thus deletion of drawables
    std::set<DrawableLight*> light_list;

    void RegisterDrawable(Drawable* d);
    void UnregisterDrawable(Drawable* d);

    void RegisterLight(DrawableLight* light);
    void UnregisterLight(DrawableLight* light);
    void ClearLights();

    ALLEGRO_BITMAP *GetLightGradient() const;
    void SetLightGradient(ALLEGRO_BITMAP* g);

    void SaveState();
    void LoadState();

    void SetBrightness(int b);
    int GetBrightness();

    void RenderLightmap();

    void RenderFrame();

};


#endif //BOX2DPLATFORM_RENDERER_H
