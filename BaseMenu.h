//
// Created by Admin on 9/9/2017.
//

#ifndef BOX2DPLATFORM_BASEMENU_H
#define BOX2DPLATFORM_BASEMENU_H


#include <Agui/Gui.hpp>
#include <map>
#include "GUIManager.h"
#include "AGUIActionListeners.h"

class BaseMenu {

    public:
    MenuButtonListener DEFAULTMENU_listener;

    explicit BaseMenu(agui::Gui *gui);
    virtual ~BaseMenu();

    virtual void Create();
    virtual void Destroy();

    std::map<std::string,agui::Widget*> id_map;   //todo make base class that does all this menu stuff

    agui::Widget* LookupWidget(std::string id);
    std::string ReverseLookupWidget(agui::Widget* w);
    void  UnregisterWidget(std::string id);
    void RegisterWidget(std::string id,agui::Widget* w);

    const std::string &GetName() const;
    void SetName(const std::string &menu_name);

    agui::Gui *GetGui() const;

    void SetGui(agui::Gui *gui);


private:
    std::string menu_name;

        agui::Gui* gui;

        agui::FlowLayout DEFAULTMENU_flow;
        agui::Button DEFAULTMENU_button2;
        agui::TextField DEFAULTMENU_text;

};


#endif //BOX2DPLATFORM_BASEMENU_H
