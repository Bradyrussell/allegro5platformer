//
// Created by Admin on 8/14/2017.
//

#ifndef BOX2DPLATFORM_BASEENTITY_H
#define BOX2DPLATFORM_BASEENTITY_H


#include <Box2D.h>
#include <allegro5/bitmap.h>

class BaseEntity {
public:

    b2Vec2 GetPosition();
    float GetX();
    float GetY();
    //float GetW();
    //float GetH();

    ALLEGRO_BITMAP* entity_sprite; // will need updated for animations

    void SetPosition(float x,float y);
    void SetPosition(b2Vec2 b2v);

    virtual ~BaseEntity();

    void SetWorld(b2World *w);
    b2World * GetWorld();

    //void Draw(); // not sure if i should really be putting this here...
    //void SetSprite(ALLEGRO_BITMAP* s);

    b2World *world;
    b2Body *body;

};


#endif //BOX2DPLATFORM_BASEENTITY_H
