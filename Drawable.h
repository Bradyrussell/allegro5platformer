//
// Created by Admin on 8/14/2017.
//

#ifndef BOX2DPLATFORM_DRAWABLE_H
#define BOX2DPLATFORM_DRAWABLE_H

//#define SCREENWIDTH 640
//#define SCREENHEIGHT 480
#define FPS_LIMIT 60
#define PIX_METER 16;

#include <allegro5/allegro5.h>
#include <set>
#include "Renderer.h"

// ugh this might fix some retarded compiler whining
class Renderer; //    is it me that is retarded? im starting to think so // update: it works so ...

class Drawable {// classes that inherit this are drawable, and this set of properties relates to that
public:
    int z_index;

    int getZindex() const;

    void setZindex(int z_index);

    ALLEGRO_BITMAP* current_sprite;
    float half_w,half_h; // store the  middle coords for later

    Renderer* owner_renderer; // stored so we can clean ourselves up on delete
    virtual ~Drawable();

    ALLEGRO_BITMAP *GetSprite() const;

    Drawable();
    void Register(Renderer *renderer);
    void Register(Renderer *renderer,int z);

    virtual void Draw();
    void SetSprite(ALLEGRO_BITMAP *s);

};


#endif //BOX2DPLATFORM_DRAWABLE_H
