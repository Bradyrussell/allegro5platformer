//
// Created by Admin on 9/9/2017.
//

#include "SoundSettingsMenu.h"

SoundSettingsMenu::SoundSettingsMenu(agui::Gui *gui) : BaseMenu(gui) {}

void SoundSettingsMenu::Create() {

    // do create here
    flow.add(&label);
    label.setSize(110,25);
    label.setText("Sound");
    label.setFontColor(agui::Color(255,0,0));
    label.setBackColor(agui::Color(66,66,66));
    label.setBlinking(true);
    label.setBlinkingInverval(.3);
    label.setReadOnly(true);
    label.setSelectable(false);
    RegisterWidget("label",&label);

    flow.add(&stop_button);
    stop_button.setSize(160,25);
    stop_button.setText("Stop Sounds");
    stop_button.addActionListener(&soundsettings_listener);
    RegisterWidget("stopsound",&stop_button);

    flow.add(&volume_slider);
    volume_slider.setSize(100,25);
    volume_slider.setMaxValue(100);
    volume_slider.setMarkerSize(agui::Dimension(10,25));
    volume_slider.addActionListener(&soundsettings_listener);
    RegisterWidget("slider",&volume_slider);

    flow.add(&close_button);
    close_button.setSize(20,20);
    close_button.setText("X");
    close_button.addActionListener(&soundsettings_listener);
    RegisterWidget("close",&close_button);

    flow.add(&save_button);
    save_button.setSize(190,25);
    save_button.setText("Apply Changes ->");
    save_button.addActionListener(&soundsettings_listener);
    RegisterWidget("apply",&save_button);

    GetGui()->add(&flow);
}
