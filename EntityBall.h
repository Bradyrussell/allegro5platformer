//
// Created by Admin on 8/14/2017.
//

#ifndef BOX2DPLATFORM_ENTITYBALL_H
#define BOX2DPLATFORM_ENTITYBALL_H

#include "BaseEntity.h"
#include "Drawable.h"

class EntityBall: public BaseEntity, public Drawable {

public:
    EntityBall(b2World *w,Renderer *renderer);
    virtual void Draw();

    virtual ~EntityBall();

};
#endif //BOX2DPLATFORM_ENTITYBALL_H
