//
// Created by Admin on 8/13/2017.
//

#include <Collision/Shapes/b2PolygonShape.h>
#include <Box2D.h>
#include <Dynamics/b2Fixture.h>
#include <allegro5/allegro.h>
#include "StaticTerrainBlock.h"
#include "UserEvents.h"
#include "GameObjectData.h"

StaticTerrainBlock::StaticTerrainBlock(b2World *w, Renderer* renderer, float x, float y, int t) {
        world = w; // grab the world reference
        Register(renderer,Z_LEVELTILES);// register it w renderer with z index leveltiles

        StaticTerrainBlock::type=t;

        b2BodyDef staticBodyDef; // define starting parameters for static terrain
        staticBodyDef.type = b2_staticBody; // static body
        staticBodyDef.position.Set(x, y); //starting position
        staticBodyDef.angle = 0; //starting angle

        //set user data
        gameobjectdata *god = new gameobjectdata;
        god->type = OBJECT_TILE;
        god->data = this;
        staticBodyDef.userData = god;

        body=world->CreateBody(&staticBodyDef);

        b2PolygonShape boxShape;
        boxShape.SetAsBox(.4,.4); // if it were .5,.5   a 1x1 entity cannot fit thru a 1x1 hole

        b2FixtureDef boxFixtureDef;
        boxFixtureDef.shape = &boxShape;
        boxFixtureDef.restitution = .2;
        boxFixtureDef.density = 1;
        boxFixtureDef.friction = 1;
        body->CreateFixture(&boxFixtureDef);
}

void StaticTerrainBlock::Cleanup() {
    world->DestroyBody(body);
}

void StaticTerrainBlock::Draw() {
        //do draw stuff
        if(body != NULL) {
                b2Vec2 pos = body->GetPosition();
                float x = pos.x * PIX_METER;
                float y = pos.y * PIX_METER;

                al_draw_rotated_bitmap(current_sprite, half_w, half_h, x + half_w, y + half_h, body->GetAngle(), 0);
        }
        return;
}

StaticTerrainBlock::~StaticTerrainBlock() {
        Cleanup();
}
