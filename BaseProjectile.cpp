//
// Created by Admin on 8/16/2017.
//

#include "BaseProjectile.h"

void BaseProjectile::Launch(b2Vec2 v) {
    body->ApplyLinearImpulseToCenter(v,true);
}

void BaseProjectile::LaunchTowards(b2Vec2 v) {
    // works great so far
    b2Vec2 me = BaseEntity::body->GetPosition();
    b2Vec2 dif = v - me;
    Launch(dif);
}

void BaseProjectile::Bounce() {
    bouncecounter++;
}

BaseProjectile::BaseProjectile(){
    bouncecounter=0;
}

BaseProjectile::~BaseProjectile() {

}

void BaseProjectile::LaunchTowardsWithForce(b2Vec2 v, float force) {
    b2Vec2 me = BaseEntity::body->GetPosition();
    b2Vec2 dif = v - me;

    b2Vec2 adjusted = b2Vec2(force*dif.x,force*dif.y);

    Launch(adjusted);
}

BaseEntity *BaseProjectile::getShooter() const {
    return shooter;
}

void BaseProjectile::setShooter(BaseEntity *shooter) {
    BaseProjectile::shooter = shooter;
}

void BaseProjectile::setBullet(bool b) {
    body->SetBullet(b);
}
