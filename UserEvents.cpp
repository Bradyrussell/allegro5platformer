//
// Created by Admin on 8/16/2017.
//

#include <iostream>
#include "UserEvents.h"



ALLEGRO_EVENT_SOURCE UserEvents::user_events;

void UserEvents::ThrowDebugEvent(void* data) {
    ALLEGRO_EVENT *new_event = new ALLEGRO_EVENT;

    new_event->type=EVENT_DEBUG;
    new_event->user.data1 = (int)data;

    al_emit_user_event(&user_events,new_event,0); // throw our user event
    return;
}

void UserEvents::ThrowCollisionBeginEvent(CollisionEventData* data) {
    ALLEGRO_EVENT *new_event = new ALLEGRO_EVENT;

    new_event->type=EVENT_BEGIN_COLLISION;
    new_event->user.data1 = (int)data;

    al_emit_user_event(&user_events,new_event,0); // throw our user event
    return;
}

void UserEvents::ThrowCollisionEndEvent(CollisionEventData* data) {
    ALLEGRO_EVENT *new_event = new ALLEGRO_EVENT;

    new_event->type=EVENT_END_COLLISION;
    new_event->user.data1 = (int)data;

    al_emit_user_event(&user_events,new_event,0); // throw our user event
    return;
}

void ::UserEvents::ThrowGUIActionEvent(UserEvents::GUIEventData *data) {
    ALLEGRO_EVENT *new_event = new ALLEGRO_EVENT;

    new_event->type=EVENT_GUI_ACTION;
    new_event->user.data1 = (int)data;

    al_emit_user_event(&user_events,new_event,0); // throw our user event
    return;
}

void ::UserEvents::ThrowProjectileHitEvent(UserEvents::ProjectileHitEventData *data) {
    ALLEGRO_EVENT *new_event = new ALLEGRO_EVENT;

    new_event->type=EVENT_PROJECTILE_HIT;
    new_event->user.data1 = (int)data;

    al_emit_user_event(&user_events,new_event,0); // throw our user event
    return;
}
