//
// Created by Admin on 8/17/2017.
//

#include <allegro5/allegro_acodec.h>
#include <iostream>
#include "SoundManager.h"

bool SoundManager::Init(int samples) {
    antiSpamCount=0;
    percentVolumeModifier=100;
    panModifier=0;
    bgm_samp_id=nullptr;

    if(al_init_acodec_addon() && al_reserve_samples(samples)) {
        std::cout << "Audio addon success!" << std::endl;
        return true;
    } else {
        std::cout << "Audio addon failure!" << std::endl;
        return false;
    }

}

void SoundManager::Play(ALLEGRO_SAMPLE *sample, float gain, float pan, float speed, ALLEGRO_PLAYMODE mode) {
    if(al_is_audio_installed()){
        if(sample != nullptr){
            al_play_sample(sample, gain*GetVolumeDecimal(), pan+GetPan(), speed, mode, NULL);
        } else {
            std::cout << "null audio sample error!"<< std::endl;
        }
    } else {
        std::cout << "audio not installed error!"<< std::endl;
    }
}

SoundManager::~SoundManager() {
    al_uninstall_audio();
}

bool SoundManager::AntiSpamCanPlay() {
    if(antiSpamCount>30){
        antiSpamCount=0;
        return true;
    }
    return false;
}

void SoundManager::AntiSpamTick() {
    antiSpamCount++;
}

void SoundManager::SetVolumePercent(int percent) {
    percentVolumeModifier = percent;
}

int SoundManager::GetVolumePercent() {
    return percentVolumeModifier;
}

float SoundManager::GetVolumeDecimal() {
    return percentVolumeModifier/100;
}

void SoundManager::SetPan(float p) {
 panModifier=p;
}

float SoundManager::GetPan() {
    return panModifier;
}

void SoundManager::PlayBGM(ALLEGRO_SAMPLE *sample, float gain, float pan, float speed, ALLEGRO_PLAYMODE mode) {
    if(al_is_audio_installed()){
        if(sample != nullptr){
            if(al_play_sample(sample, gain*GetVolumeDecimal(), pan+GetPan(), speed, mode, bgm_samp_id)){
                //std::cout << "Playing sample."<< std::endl;
            }else {
                std::cout << "al_play_sample return false error!"<< std::endl;
            }
        } else {
            std::cout << "null audio sample error!"<< std::endl;
        }
    } else {
        std::cout << "audio not installed error!"<< std::endl;
    }
}

void SoundManager::StopBGM() {
    if(bgm_samp_id != nullptr){
        al_stop_sample(bgm_samp_id);
    }
}

