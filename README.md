# Allegro 5 / Box2D Platformer Engine #
_by Brady Russell_

### Allegro 5 / Box2D Platformer Engine ###

* This is a personal project, wherein I am experimenting with 2D platformer engine design. Libraries used are [allegro5](http://liballeg.org/), [AGUI (allegro5 GUI library)](https://github.com/jmasterx/Agui), and [Box2D](http://box2d.org/).
* As of `8/30/17` this project is entirely private, however I intend to share this repository in the future.
* Version 0.0.2

~Today I learned about merge conflicts.~