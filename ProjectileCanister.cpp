//
// Created by Admin on 8/17/2017.
//

#include <iostream>
#include "ProjectileCanister.h"
#include "GameObjectData.h"

ProjectileCanister::ProjectileCanister(b2World *w, Renderer *renderer) {
    world = w; // grab the world reference
    Register(renderer,Z_GENERICENTITY); // z index genericentity

    b2BodyDef canBodyDef; // define starting parameters for player body
    canBodyDef.type = b2_dynamicBody; // dynamic body
    canBodyDef.position.Set(0, 0); //starting position
    canBodyDef.angle = 0; //starting angle
    //canBodyDef.bullet = true;

    canBodyDef.fixedRotation = false;

    //set user data
    auto *god = new gameobjectdata;
    god->type = OBJECT_PROJECTILECANISTER;
    god->data = this;
    god->projectile = true;
    canBodyDef.userData = god;

    body=world->CreateBody(&canBodyDef);

    b2CircleShape canShape;
    canShape.m_p.Set(0,0);
    canShape.m_radius = .4;

    b2FixtureDef circleFixtureDef;
    circleFixtureDef.shape = &canShape;
    circleFixtureDef.restitution = .5;
    circleFixtureDef.density = 1;
    body->CreateFixture(&circleFixtureDef);

    canlight = new DrawableLight(renderer,body->GetPosition(),0.3);
}

void ProjectileCanister::Draw() {
    //do draw stuff
    if(body != nullptr) {
        b2Vec2 pos = body->GetPosition();
        canlight->setPosition(pos);
        float x = pos.x * PIX_METER;
        float y = pos.y * PIX_METER;
        al_draw_rotated_bitmap(current_sprite, half_w, half_h, x + half_w, y + half_h, body->GetAngle(), 0);

        if(!body->IsAwake()) {
          //std::cout << "Canister has come to rest." << std::endl;
        }
    }
}

ProjectileCanister::~ProjectileCanister() { // calling anything here crashes ? lmao
    if(canlight != nullptr){
        //BaseProjectile::~BaseProjectile();
        delete(canlight);
        std::cout << "ProjectileCanister::~ProjectileCanister()" << std::endl;
    }
}
