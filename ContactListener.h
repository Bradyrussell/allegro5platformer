//
// Created by Admin on 8/16/2017.
//

#ifndef BOX2DPLATFORM_PLAYERFOOTCONTACTLISTENER_H
#define BOX2DPLATFORM_PLAYERFOOTCONTACTLISTENER_H


#include <Box2D.h>
#include "Player.h"

class ContactListener : public b2ContactListener {
    Player* p;

public:
    ContactListener(Player *p);
    void BeginContact(b2Contact* contact);
    void EndContact(b2Contact* contact);
};


#endif //BOX2DPLATFORM_PLAYERFOOTCONTACTLISTENER_H
