//
// Created by Admin on 8/14/2017.
//

#include <Box2D.h>
#include <iostream>
#include <set>
#include "Level.h"
#include "StaticTerrainBlock.h"


int Level::GetTile(int x, int y) {
    return level_map[x][y];
}

void Level::SetTile(int x, int y, int t) {
    level_map[x][y] = t;
    return;
}

bool Level::Load(const char *f) {
    config = al_load_config_file(f);

    if(config==NULL){
        std::cout << "Level.Load() no such file! " << f << std::endl;
        return false;
    }

    try {

        name = al_get_config_value(config, "", "name");
        spawnx = atof(al_get_config_value(config, "", "spawnx"));
        spawny = atof(al_get_config_value(config, "", "spawny"));

        for (int x = 0; x < 40; x++) { // this might be broken AF
            for (int y = 0; y < 30; y++) {
                char xbuf[16];
                char ybuf[16];
                itoa(x, xbuf, 10);
                itoa(y, ybuf, 10);
                Level::level_map[x][y] = atoi(al_get_config_value(config, xbuf, ybuf));
            }
        }
    } catch(const std::exception& e){
        std::cout << "Level.Load() failed! " << e.what() << std::endl;
        return false;
    }

    return true;
}

bool Level::Save(const char *f) {
    //if(config==NULL){ // does it exist?
    //    std::cout << "Level.Save() creating file! " << f << std::endl;
        config=al_create_config(); // probably should just always create new one
    //}

    al_set_config_value(config, "", "name", name.c_str());

    char sxbuf[16];
    char sybuf[16];
    itoa(spawnx, sxbuf, 10);
    itoa(spawny, sybuf, 10);

    al_set_config_value(config, "", "spawnx", sxbuf);
    al_set_config_value(config, "", "spawny", sybuf);

    for (int x = 0; x < 40; x++) { // this might be broken AF
        for (int y = 0; y < 30; y++) {
            char xbuf[16];
            char ybuf[16];
            char valbuf[32];
            itoa(x, xbuf, 10);
            itoa(y, ybuf, 10);
            itoa(Level::level_map[x][y],valbuf,10);
            al_set_config_value(config, xbuf, ybuf, valbuf);
        }
    }

    al_save_config_file(f,config);
    return false;
}

void Level::Create(b2World *w, Renderer* r, std::set<StaticTerrainBlock*> *stbset) { // creates the level in the given world
    for (int x = 0; x < 40; x++) { // this might be broken AF
        for (int y = 0; y < 30; y++) {
            int type = Level::level_map[x][y];

            if (type != 0) {
            // create new static terrain block of type @ x ,y
            StaticTerrainBlock *alloc_stb = new StaticTerrainBlock(w, r, x, y, type); // generate blocks
            stbset->insert(alloc_stb);
            }
        }
    }

}

void Level::GenerateFromSet(std::set<StaticTerrainBlock *> *stbset) {
    Zero();
    for(StaticTerrainBlock * s:*stbset){
        b2Vec2 pos = s->body->GetPosition();
        int xpos = pos.x;  // bad things might happen from erroneous rounding. oops
        int ypos = pos.y;

        Level::level_map[xpos][ypos] = s->type;
    }
    return;
}

void Level::Zero() {
    for (int x = 0; x < 40; x++) { // this might be broken AF
        for (int y = 0; y < 30; y++) {
            Level::level_map[x][y] = 0;
        }
    }
    return;
}
