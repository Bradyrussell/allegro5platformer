//
// Created by Admin on 8/17/2017.
//

#ifndef BOX2DPLATFORM_GAMEOBJECTDATA_H
#define BOX2DPLATFORM_GAMEOBJECTDATA_H

enum gameobjecttype {
    OBJECT_DEBUG,
    OBJECT_PLAYER,
    OBJECT_ENTITYBALL,
    OBJECT_PROJECTILECANISTER,
    OBJECT_TILE,
    OBJECT_PROJECTILESMALLBULLET
};

enum physicsfiltergroup{
    FILTER_PLAYER=-1337
};

struct gameobjectdata { // box2d bodies userdata get set to this
    gameobjecttype type;
    void* data;
    bool projectile=false;
};


#endif //BOX2DPLATFORM_GAMEOBJECTDATA_H
