//
// Created by Admin on 8/17/2017.
//

#ifndef BOX2DPLATFORM_DRAWABLELIGHT_H
#define BOX2DPLATFORM_DRAWABLELIGHT_H

#include <Box2D.h>
#include <allegro5/bitmap.h>
#include "Renderer.h"

//forward declare renderer
class Renderer;

class DrawableLight {
public:
    b2Vec2 position;
    Renderer* owner_renderer; // stored so we can clean ourselves up on delete
    ALLEGRO_BITMAP* gradient;

    float intensity;

    const b2Vec2 &getPosition() const;

    void setPosition(const b2Vec2 &position);

    float getIntensity() const;

    void setIntensity(float intensity);

    virtual ~DrawableLight();

    DrawableLight(Renderer *renderer);
    DrawableLight(Renderer *renderer,b2Vec2 position, float intensity);

    Renderer *getRenderer() const;

    void setRenderer(Renderer *owner_renderer);

    ALLEGRO_BITMAP *getGradient() const;

    void setGradient(ALLEGRO_BITMAP *gradient);

    void Register(Renderer *renderer);
    virtual void Draw();
};


#endif //BOX2DPLATFORM_DRAWABLELIGHT_H
