//
// Created by Admin on 8/18/2017.
//

#include <iostream>
#include "GUIManager.h"




void GUIManager::Init() {
    try {
        //Set the image loader
        agui::Image::setImageLoader(new agui::Allegro5ImageLoader);

        //Set the font loader
        agui::Font::setFontLoader(new agui::Allegro5FontLoader);

        //Instance the input handler
        inputHandler = new agui::Allegro5Input();

        //Instance the graphics handler
        graphicsHandler = new agui::Allegro5Graphics();

        //Allegro does not automatically premultiply alpha so let Agui do it
        agui::Color::setPremultiplyAlpha(true);

        //Instance the gui
        gui = new agui::Gui();

        //Set the input handler
        gui->setInput(inputHandler);

        //Set the graphics handler
        gui->setGraphics(graphicsHandler);

        defaultFont = agui::Font::load("image/font.ttf", 10);

        //Setting a global font is required and failure to do so will crash.
        agui::Widget::setGlobalFont(defaultFont);
    } catch(agui::Exception &e){
        std::cout << "AGUI Exception: " << e.getMessage() << std::endl;
    }
}

GUIManager::~GUIManager() {
    Clear();

    delete gui;
    gui = nullptr;
    delete inputHandler;
    delete graphicsHandler;
    inputHandler = nullptr;
    graphicsHandler = nullptr;

    //delete defaultFont;  this causes crash
    //defaultFont = nullptr;
}

void GUIManager::Clear() {
//    gui->getTop()->clear();
//    active_menus.clear();
    for(std::pair<std::string, BaseMenu*>val:active_menus) {
        RemoveMenu(val.first);
    }

    active_menus.clear();
}

void GUIManager::Draw() {
    gui->render();
}

void GUIManager::HandleInput(ALLEGRO_EVENT evt) {
    inputHandler->processEvent(evt);
}

void GUIManager::Update() {
    gui->logic();
}

bool GUIManager::isUsingMouse() const {
    return menu_has_mouse;
}

void GUIManager::setUsingMouse(bool menu_has_mouse) {
    GUIManager::menu_has_mouse = menu_has_mouse;
}

bool GUIManager::isUsingKeys() const {
    return menu_has_keys;
}

void GUIManager::setUsingKeys(bool menu_has_keys) {
    GUIManager::menu_has_keys = menu_has_keys;
}

void GUIManager::setUsingMouseKeys(bool menu_has_both) {
    setUsingMouse(menu_has_both);
    setUsingKeys(menu_has_both);
}

BaseMenu *GUIManager::CreateMenu(BaseMenu *menu, const char *name) {
    menu->Create();
    menu->SetName(name);
    active_menus.insert(std::pair<std::string,BaseMenu*>(name,menu));
}

BaseMenu *GUIManager::GetMenu(std::string name) {
    try {
        return active_menus.at(name);
    } catch(std::out_of_range &e) {
        std::cout << "GUIManager::GetMenu out_of_range exception. Key \"" << name << "\" doesn't exist! Exception data:" << e.what() << std::endl;
        return nullptr;
    }
}

void GUIManager::RemoveMenu(std::string name) {
    auto ptr2delete = GetMenu(name);

    try {
        active_menus.erase(name);
        if (ptr2delete != nullptr) {
            std::cout << "Deleting non-null Menu ptr." << std::endl;
            delete ptr2delete;
        }
    } catch(std::out_of_range &e) {
        std::cout << "GUIManager::RemoveMenu out_of_range exception. Exception data:" << e.what() << std::endl;
        return;
    }



}

void GUIManager::ValidateMenus() { // iterate thru menus and verify they are alive
    //std::set<std::string> invalid_menus;

    // find any menus with null ptrs
    for(std::pair<std::string, BaseMenu*>val:active_menus){
        std::cout << "Validating '" << val.first << "' menu..." << std::endl;
        if(val.second==nullptr){
            std::cout << "Invalid menu ptr '" << val.first << "'!" << std::endl;
            RemoveMenu(val.first);
        }
    }
}

std::list<std::string> GUIManager::GetMenuList() {
    std::list<std::string> menulist;

    for(std::pair<std::string, BaseMenu*>val:active_menus){ // get the names of entire list and return
        menulist.push_back(val.first);
    }

    return menulist;
}

int GUIManager::GetMenuCount() {
    return active_menus.size();
}


